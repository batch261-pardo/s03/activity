package com.zuitt;

import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);

        try{
            int answer = 1;
            int counter = 1;
            System.out.println("Input an integer whose factorial will be computed");
            int num = in.nextInt();

            if(num <= 0){
                System.out.println("Input should be equal to 0 and not negative numbers.");
            }else{
                while(counter <= num) {
                    answer *= counter;
                    counter++;
                }
                System.out.println("While Loop.");
                System.out.println("The factorial of " + num + " is " + answer);
            }
        }
        catch(Exception e){
            System.out.println("Invalid Input");
            e.printStackTrace();
        }finally {
            in.close();
        }

    }
}
